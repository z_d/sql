import psycopg2
command = input("input you command:")

connection = psycopg2.connect(
  database="study_psql",
  user="postgres",
  password="postgres",
  host="ip-address",
  port="5432"
)

print("Database opened successfully")

cursor = connection.cursor()
cursor.execute(f'{command}')
record = cursor.fetchall()
for row in record:
	#newrow=row.replace('\n','')
	print(*row)
cursor.close()
connection.close()
