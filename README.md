SQL requests examples.

Example docker-container.
 
```
docker run --name study_psql \
-p 5432:5432 \
-e POSTGRES_USER=postgres \ 
-e POSTGRES_PASSWORD=postgres \
-e POSTGRES_DB=study_psql \
-e PGDATA=/var/lib/postgresql/data/pgdata \
-e POSTGRES_INITDB_ARGS="--data-checksums" \
-v /srv/postgres/:/var/lib/postgresql/data \
-d postgres:11
```
* Creating dump postgres database.

```
pg_dump -U postgres -W postgres -h localhost > postgres.dump
```

* Restore postgres database at dump.

```
psql -h localhost -U postgres -W postgres -d study_psql -f "/your_directory/postgres.dump"
```
