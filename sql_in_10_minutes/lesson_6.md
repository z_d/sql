# **Lesson #6**

###### Фильтрация с использованием метасимволов.
###### Чтобы использовать метасимволы в условиях отбора строк, необходимо задействовать ключевое слово LIKE.

```
SELECT prod_id, prod_name FROM Products WHERE prod_name LIKE 'Fish%'; #Знак % заставляет СУБД принимать все символы после слова Fish независимо от их кол-ва.
```
```
SELECT prod_id, prod_name FROM Products WHERE prod_name LIKE '%bean bag%';  #Метасимволы могут встречаться в любом месте шаблона поиска.
```
```
SELECT prod_id, prod_name FROM Products WHERE prod_name LIKE 'F%y%';   #Поиск названий, начинающихся на F и кончающихся на y.
```
```
SELECT prod_id, prod_name FROM Products WHERE prod_name LIKE '__ inch teddy bear%'; # Метасимвол '_' учитывает один символ, а не множество.
```
```
SELECT cust_contact FROM Customers WHERE cust_contact LIKE '[JM]%' ORDER BY cust_contact; # Метасимвол '[]' служит для указания набора символов.

https://postgrespro.ru/docs/postgresql/11/arrays
```
