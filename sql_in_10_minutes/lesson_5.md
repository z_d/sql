# **Leasson #5**

###### Расширенная фильтрация данных.

```
SELECT prod_id, prod_price, prod_name FROM Products WHERE vend_id = 'DLL01' AND prod_price <=4;      #Комбинирование условий WHERE. Оператор AND.
```
```
SELECT prod_name, prod_price FROM Products WHERE vend_id = 'DLL01' OR vend_id = 'BRS01' ORDER BY prod_price DESC;   #Может использоваться ORDER BY
```
```
SELECT prod_name, prod_price FROM Products WHERE vend_id = 'DLL01' OR vend_id = 'BRS01';         #Комбинирование условий WHERE. Оператор OR.
```

###### При фильтрации приоритет имеет оператор AND, затем OR.

```
SELECT prod_name, prod_price FROM Products WHERE (vend_id = 'DLL01' OR vend_id = 'BRS01') AND prod_price >= 10; #Использование скобок для фильтрации.
```

###### Скобки имеют еще больший приоритет чем AND или OR. Скобки используются для более точной группировки условий.

```
SELECT prod_name, prod_price FROM Products WHERE vend_id IN ('DLL01','BRS01') ORDER BY prod_name; #IN служит для указания диапозона условий, любое из которых может быть выполнено. Зачения в скобках перечисляются через запятую.
```
```
SELECT prod_name FROM Products WHERE NOT vend_id = 'DLL01' ORDER BY prod_name; #Извлекаются товары всех поставщиков, кроме DLL01.
```
```
SELECT prod_name FROM Products WHERE vend_id <> 'DLL01' ORDER BY prod_name; #Тот же запрос без NOT.
```
