# **Lesson #4**

###### Фильтрация данных.

```
SELECT prod_name, prod_price FROM Products WHERE prod_price=3.49;  #Использование предложения WHERE для фильтрации. Все товары равны 3.49.
```
```
SELECT prod_name, prod_price FROM Products WHERE prod_price < 10;  #Сравнение с однозначным значением.Все товары дешевле 10.
```
```
SELECT prod_name, prod_price FROM Products WHERE prod_price <=10;  #Сравнение с однозначным значением.Все товары дешевле или равны 10.
```
```
SELECT vend_id, prod_price FROM Products WHERE vend_id <> 'DLL01';  #Проверка на неравенство. Все товары не изготовленные 'DLL01'.
```
```
SELECT vend_id, prod_price FROM Products WHERE vend_id != 'DLL01';  # ----////----
```
```
SELECT prod_name, prod_price FROM Products WHERE prod_price BETWEEN 5 AND 10; #Сравнение с диапозоном значений.
```
```
SELECT prod_name FROM Products WHERE prod_price IS NULL; #Проверка на отсутствие значения. Список товаров без цен.
```
```
SELECT cust_name FROM Customers WHERE cust_email IS NULL;  #Проверка адресов e-mail. 
```
