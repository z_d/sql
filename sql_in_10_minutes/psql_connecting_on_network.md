```
В файле /etc/postgresql/11/main/postgresql.conf 

находим строку

listen_addresses = '*'

Вместо 'localhost' ставим звездочку - * и разрешаем все подключения.

В файле /etc/postgresql/11/main/pg_hba.conf

Добавляем в конце файла настройки для сети.

host all  all   192.168.1.0/24  md5

```

Открыть порт в фаерволе 

```
sudo ufw allow 5432/tcp

sudo ufw reload
```

Проверить подключение

```
psql -h 192.168.1.20 -U userpsql
```

