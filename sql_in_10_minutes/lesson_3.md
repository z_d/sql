# **Lesson #3**

```
SELECT prod_name FROM Products ORDER BY prod_name;                                    #Сортировка в алфавитном порядке.
```
```
SELECT prod_id, prod_price, prod_name FROM Products ORDER BY prod_price, prod_name;   #Сортировка по нескольким столбцам
```
```
SELECT prod_id, prod_price, prod_name FROM Products ORDER BY 2,3;                     #Сортировка по положению(номеру) столбца
```
```
SELECT prod_id, prod_price, prod_name FROM Products ORDER BY prod_price DESC;         #Указание направления сортировки. Сначала самые дорогие товары.
```
```
SELECT prod_id, prod_price, prod_name FROM Products ORDER BY prod_price DESC, prod_name; #Сортировка по порядку убывания цен и названию.
```
```
SELECT prod_id, prod_price, prod_name FROM Products ORDER BY prod_price ASC, prod_name; #Сортировка по порядку возрастания цен и названию.
```

DESC - descending(низходящий, по убыванию)

ASC - ascending(восходящий, по возрастанию)
