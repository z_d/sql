# **Lesson #7**

###### Создание вычисляемых полей

```
SELECT vend_name || ' ('|| vend_country ||') ' FROM Vendors ORDER BY vend_name; #Конкатенация полей - комбинирование.
```

```
SELECT RTRIM(vend_name) || ' ('|| RTRIM(vend_country) ||') ' FROM Vendors ORDER BY vend_name; # RTRIM - отбрасывает все пробелы справа. LTRIM - слева, TRIM - пробелы слева и справа.
```

```
SELECT RTRIM(vend_name) || ' ('|| RTRIM(vend_country) ||') ' AS vend_title FROM Vendors ORDER BY vend_name; #Псевдонимы присваиваются с помощью ключевого слова AS.
```

```
SELECT prod_id, quantity, item_price, quantity * item_price AS expanded_price FROM OrderItems WHERE order_num = 20008;  #Выполнение математических вычислений. Вычисляемое поле expanded_price - это произведение от quantity * item_price.
```
