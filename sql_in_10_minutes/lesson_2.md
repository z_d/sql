# **Lesson #2**

```
SELECT prod_name FROM Products;  #Извлечение отдельного столбца из таблицы.
```
```
SELECT prod_id, prod_name, prod_price FROM Products; #Извлечение нескольких столбцов из таблицы.
```
```
SELECT * FROM Products; #Извлечение всех столбцов из таблицы.
```
```
SELECT DISTINCT vend_id FROM Products; #Извлечение уникальных строк из таблицы.
```
```
SELECT prod_name FROM Products LIMIT 5; #Ограничение результата запроса первыми 5-ю строками.
```
```
SELECT prod_name FROM Products LIMIT 5 OFFSET 5; #Ограничение результата запроса первыми 5-ю строками, начиная с 5-ой строки.
```

```
SELECT prod_name --this is comment
FROM Products;                                   #Встраиваемый комментарий
```
```
#this is comment
SELECT prod_name 
FROM Products;                                   #Встраиваемый комментарий
```
```
/* SELECT prod_name, vend_id
FROM Products; */
SELECT prod_name 
FROM Products;                                   #Встраиваемый комментарий
```
